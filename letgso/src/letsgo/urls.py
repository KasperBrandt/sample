from django.conf.urls import patterns, include, url
from webinterface.views import CarDetailView, CarListView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^blog/', include('blog.urls')),
    url(r'id/(?P<car_id>\d+)/$', CarDetailView.as_view(), name="region"),
    url(r'(?P<region>\w+)/$', CarListView.as_view(), name="region"),
    url(r'^$', CarListView.as_view(), name="home"),


    url(r'^admin/', include(admin.site.urls)),


)
