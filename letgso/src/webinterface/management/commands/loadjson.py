from django.core.management.base import BaseCommand
from webinterface.models import Car
import json
import requests


class Command(BaseCommand):
    help = 'Load the json file into the database.'

    def handle(self, *args, **options):
        url = "https://letsgo.dk/sample.json"
        resp = requests.get(url=url)
        data = json.loads(resp.text)

        cars = data["cars"]

        for car in cars:
            new_car = Car(**car)
            new_car.save()
