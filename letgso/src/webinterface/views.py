from django.views.generic import TemplateView
from webinterface.models import Car
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

KWARGS_REGION = "region"


class CarListView(TemplateView):
    template_name = 'list.html'

    def get_context_data(self, **kwargs):
        context = super(CarListView, self).get_context_data(**kwargs)
        if KWARGS_REGION in kwargs:
            region = self.kwargs['region'].lower()
            all_cars = Car.objects.filter(region=region)
        else:
            all_cars = Car.objects.all()

        # Paginator
        paginator = Paginator(all_cars, 10)

        page = self.request.GET.get('page')
        try:
            paginated_cars = paginator.page(page)
        except PageNotAnInteger:
            paginated_cars = paginator.page(1)
        except EmptyPage:
            paginated_cars = paginator.page(paginator.num_pages)

        context.update({
            'cars': paginated_cars,
        })
        return context


class CarDetailView(TemplateView):
    template_name = 'detail.html'

    def get_context_data(self, **kwargs):
        context = super(CarDetailView, self).get_context_data(**kwargs)
        car_id = self.kwargs['car_id'].lower()
        car = Car.objects.get(id=car_id)

        context.update({
            'car': car,
        })
        return context
