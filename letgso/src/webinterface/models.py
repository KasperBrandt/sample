from django.db import models


class Car(models.Model):
    vehicle_type_image = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    fuel_type = models.IntegerField()
    license_plate = models.CharField(max_length=255)
    health = models.IntegerField(null=True, blank=True)
    has_logo = models.BooleanField()
    year = models.IntegerField()
    region = models.CharField(max_length=255)
    model = models.CharField(max_length=255)
    vehicle_type = models.CharField(max_length=255)
    make = models.CharField(max_length=255)
