# LetsGo sample

Fork this project and send us a pull request.

## The task

You need to create a simple python webservice written in Django, to show and
filter anonymised data about some of the cars in LetsGo's fleet. You are free
to use any 3rd party library that you want.

You can find the data sample at https://letsgo.dk/sample.json

The following endpoints need to be implemented.

### /cars/

This endpoint should return the first 10 cars ordered with the newest cars
first.

### /cars/?page=2

The cars should be paginated where **page** in the url above should return the
next 10 cars.

### /cars/greve/

This endpoint should return the cars in the greve region with the newest cars
first.

### /cars/id/

This endpoint should return details about a single car.

## Keep in mind

When you write remember to test and document (the why not the how).
